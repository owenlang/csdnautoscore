#include "mainwindow.h"
#include <QDebug>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QTimer>
#include <QMessageBox>
#include <QTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
  , _requestType(NONE), _pageCount(0)
{
    setupUi(this);
    initData();
}

void MainWindow::initData()
{
    connect(loginBtn, SIGNAL(clicked()), this, SLOT(login()));
    connect(logoutBtn, SIGNAL(clicked()), this, SLOT(logout()));
    connect(listBtn, SIGNAL(clicked()), this, SLOT(getList()));
    connect(scoreBtn, SIGNAL(clicked()), this, SLOT(score()));

    _nam = new QNetworkAccessManager(this);
    connect(_nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

    connect(this, SIGNAL(getDownloadPage()), this, SLOT(getAllResource()));

    tableWidget->setColumnCount(2);
    tableWidget->setColumnWidth(0, 300);
    tableWidget->setColumnWidth(1, 300);

    QStringList header;
    header << QString::fromLocal8Bit("资源名称") << QString::fromLocal8Bit("资源路径");
    tableWidget->setHorizontalHeaderLabels(header);

    passwordEdit->setStyleSheet("lineedit-password-character: 42");

    infoLabel->setText(QString::fromLocal8Bit("欢迎使用本程序，有任何问题或建议请联系owen@linxiaolog.com,谢谢！\n"));
}

void MainWindow::login()
{
    if (userEdit->text().isEmpty() || passwordEdit->text().isEmpty()) {
        QMessageBox::warning(this, tr("Warning"), tr("Please input user name and password!"));
        return;
    }
    _user = userEdit->text().trimmed();
    _password = passwordEdit->text().trimmed();

    bool ok;
    qsrand(QTime::currentTime().toString("hhmmss").toInt(&ok, 10));

    QNetworkRequest request;
    request.setUrl(QUrl("http://passport.csdn.net/ajax/accounthandler.ashx?t=log&u=" + _user + "&p=" + _password
                        + "&remember=1&f=http%3A%2F%2Fwww.csdn.net%2F&rand=0.888385001922586" + QString("%1").arg(qrand()%10)));
    request.setRawHeader("Host", "passport.csdn.net");
    request.setRawHeader("Referer", "https://passport.csdn.net/account/loginbox?callback=logined&hidethird=1&from=http%3a%2f%2fwww.csdn.net%2f");
    request.setRawHeader("Connection", "keep-alive");

    QNetworkReply *reply = _nam->get(request);
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(slotError(QNetworkReply::NetworkError)));
    _requestType = LOGIN;
}

void MainWindow::logout()
{
    QNetworkRequest request;
    request.setUrl(QUrl("http://passport.csdn.net/account/logout"));
    _nam->get(request);

    int row = tableWidget->rowCount();
    for (int i = row; i >= 0; i--) {
        tableWidget->removeRow(i);
    }
}

void MainWindow::delayMs(qint32 ms)
{
    QTime dieTime = QTime::currentTime().addMSecs(ms);
    while (QTime::currentTime() < dieTime) {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
    }
}
void MainWindow::score()
{
    int row = tableWidget->rowCount();
    for (int i = row-1; i >= 0; --i) {
        QString url = tableWidget->takeItem(i, 1)->text();
        QRegExp rx_id(".*http://csdn.net/.*/.*/(.*)");
        rx_id.exactMatch(url);
        QString id = rx_id.cap(1);

        QNetworkRequest request;
        request.setUrl(QUrl("http://download.csdn.net/index.php/comment/post_comment?jsonpcallback=jsonp1390823742640&sourceid=" + id
                + "&content=%E5%A4%9A%E8%B0%A2%E5%88%86%E4%BA%AB%EF%BC%81&rating=4&t=1390826445797"));
        request.setRawHeader("Connection", "keep-alive");

        QNetworkReply *reply = _nam->get(request);
        connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(slotError(QNetworkReply::NetworkError)));
        _requestType = SCORE;

        qDebug() << QString::fromLocal8Bit("正在评价: id=") << id << "; title=" << tableWidget->takeItem(i, 0)->text();
        tableWidget->removeRow(i);

        if (0 == i) {
            infoLabel->setText(QString::fromLocal8Bit("所有资源评价完成！"));
            break;
        }
        delayMs(62000);
    }
}

void MainWindow::getList()
{
    QNetworkRequest request;
    request.setUrl(QUrl("http://download.csdn.net/my/downloads"));
    request.setRawHeader("Connection", "keep-alive");

    QNetworkReply *reply = _nam->get(request);
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(slotError(QNetworkReply::NetworkError)));
    _requestType = GET_PAGE_COUNT;
}

void MainWindow::loginProcess(const QString &res)
{
    QRegExp rx_email(".*\"email\":\"(.*)\".*");
    rx_email.setMinimal(true);
    int pos = 0;
    if ((pos=rx_email.indexIn(res, 0)) != -1) {
        _userInfo += "Email:";
        _userInfo += rx_email.cap(1);
    }
    QRegExp rx_reg_time(".*\"registerTime\":\"(.*)\".*");
    rx_reg_time.setMinimal(true);
    if ((pos=rx_reg_time.indexIn(res, 0)) != -1) {
        _userInfo += QString::fromLocal8Bit(",注册时间:");
        _userInfo += rx_reg_time.cap(1);
    }

    QRegExp rx_last_login_time(".*\"lastLoginTime\":\"(.*)\".*");
    rx_last_login_time.setMinimal(true);
    if ((pos=rx_last_login_time.indexIn(res, 0)) != -1) {
        _userInfo += QString::fromLocal8Bit(",最后登录时间:");
        _userInfo += rx_last_login_time.cap(1);
    }

    infoLabel->setText(QString::fromLocal8Bit("登录成功！") + _userInfo);

}

void MainWindow::getPageProcess(const QString &res)
{
    //get resource name, id, add to list
    QRegExp rx("<dt>\\s+<span class=\"share\">(.*)</span>\\s+</dt>");
    rx.setMinimal(true);
    QStringList strlist;
    int pos = 0;
    while ((pos = rx.indexIn(res, pos)) != -1) {
        if (rx.cap(1).contains("recom_plese")) {
            strlist << rx.cap(1);
        }
        pos += rx.matchedLength();
    }

    foreach (QString str, strlist) {
        QRegExp rx_extract(".*</span>\\s+<a href=\"(/detail/.*/\\d+)\">(.*)</a>.*");
        rx_extract.exactMatch(str);
        QString path = "http://csdn.net" + rx_extract.cap(1);
        QString name = rx_extract.cap(2);

        int row = tableWidget->rowCount();
        tableWidget->insertRow(row);
        tableWidget->setItem(row, 0, new QTableWidgetItem(name));
        tableWidget->setItem(row, 1, new QTableWidgetItem(path));
    }
}

void MainWindow::replyFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError) {
        QString res = QString::fromUtf8(QString(reply->readAll()).toLatin1());
        switch (_requestType) {
        case LOGIN:
            {
                if (!res.contains("email")) {
                    _requestType = NONE;
                    infoLabel->setText(QString::fromLocal8Bit("登录失败！"));
                    return;
                }

                loginProcess(res);
            }
            break;
        case GET_PAGE_COUNT:
            {
                QRegExp exp(QString::fromLocal8Bit(".*共(\\d+)页.*"));
                if (exp.exactMatch((res))) {
                    bool ok = false;
                    _pageCount = exp.cap(1).toInt(&ok, 10);
                    if (0 == _pageCount) {
                        infoLabel->setText(QString::fromLocal8Bit("读取页数错误！"));
                        return;
                    }
                    infoLabel->setText(QString::fromLocal8Bit(" 正在获取未评价资源..."));
                    //start get download pages
                    _requestType = NONE;
                    emit getDownloadPage();
                    return;
                }
            }
            break;
        case GET_DOWNLOAD_PAGE:
            {
                getPageProcess(res);
                //get next page.
                _requestType = NONE;
                emit getDownloadPage();
                return;
            }
            break;
        case SCORE:
            infoLabel->setText(QString::fromLocal8Bit("评价完成，等待评价下一条(每次评价自动间隔60秒!)..."));
            break;
        }
    }
    _requestType = NONE;
    reply->deleteLater();
}

void MainWindow::getAllResource()
{
    static int i = 1;
    if (i > _pageCount) {
        i = 0;
        infoLabel->setText(QString::fromLocal8Bit("获取完成！"));
        return;
    }
    if (i <= _pageCount) {
        QNetworkRequest request;
        request.setUrl(QUrl("http://download.csdn.net/my/downloads/" + QString("%1").arg(i)));
        request.setRawHeader("Host", "download.csdn.net");
        request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        request.setRawHeader("Connection", "keep-alive");
        QNetworkReply *reply = _nam->get(request);
        connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(slotError(QNetworkReply::NetworkError)));
        _requestType = GET_DOWNLOAD_PAGE;
        ++i;
    }
}

void MainWindow::slotError(QNetworkReply::NetworkError)
{
    switch (_requestType) {
    case LOGIN:
        infoLabel->setText(QString::fromLocal8Bit("登录发生错误！"));
        break;
    case GET_PAGE_COUNT:
        infoLabel->setText(QString::fromLocal8Bit("读取页数错误！"));
        break;
    case GET_DOWNLOAD_PAGE:
        infoLabel->setText(QString::fromLocal8Bit("获取资源错误！"));
        break;
    case SCORE:
        infoLabel->setText(QString::fromLocal8Bit("评价出错！"));
        break;
    }
    _requestType = NONE;
}

MainWindow::~MainWindow()
{
    delete _nam;
}
