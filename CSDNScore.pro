#-------------------------------------------------
#
# Project created by QtCreator 2014-01-26T13:42:17
#
#-------------------------------------------------

QT       += core gui network

TARGET = CSDNScore
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
