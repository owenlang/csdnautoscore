#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include <QNetworkReply>
#include <QMap>

class LoginDialog;
class QNetworkAccessManager;
class QTimer;

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

signals:
    void getDownloadPage();

private slots:    
    void login();
    void logout();
    void getList();
    void score();
    void replyFinished(QNetworkReply *reply);
    void slotError(QNetworkReply::NetworkError);
    void getAllResource();

private:
    void initData();
    void delayMs(qint32);
    void loginProcess(const QString &res);
    void getPageProcess(const QString &res);

    QString _user;
    QString _password;
    QNetworkAccessManager *_nam;
    QString _userInfo;
    qint32 _pageCount;

    typedef enum {
        NONE,
        LOGIN,
        GET_PAGE_COUNT,
        GET_DOWNLOAD_PAGE,
        SCORE
    }RequestType;
    RequestType _requestType;
};

#endif // MAINWINDOW_H
